# Demonstration: Accuracy of two plain CNNs on CIFAR-10 with different depth
This code is adapted from 4 sources:
- https://github.com/lambdal/TensorFlow2-tutorial
- https://github.com/tensorflow/modelAs/blob/master/official/benchmark/modelAs/resnet_cifar_main.py
- https://github.com/keras-team/keras/blob/master/examples/cifar10_resnet.py
- https://github.com/chao-ji/tf-resnet-cifar10

The project is currently not working entirely as designed, with some errors. I will not fix this in the near future. More experienced users of tensorflow will easily see what is wrong and can fix it themselves. For beginners, I recommend checking out some official tutorials first.

## Requirements:
- Tensorflow Version >= 2 and all its requirements
- up-to-date GPU drivers
- Matplotlib
- Python >= 3.6
- OS: Code written and tested on Windows 10 Home 64-bit, no guarantee for other OSes
- numpy
- internet connection etc.

## Run 
- In Windows Powershell: execute 'python ./plainnet_cifar.py'

## Expected results
- After both networks have finished training, a plot will display their respective training- and test-accuracy
- File Figure_4a.png shows results that were achieved using a Nvidia 1060 6GB

## Resnet
Folder resnetVersion contains code for comparing two residual networks (He et al.2016) in the same setting.
Usage: Navigate in the subfolder, then execute 'python ./resnet_cifar.py' via Windows Powershell.

B. Luedtke
