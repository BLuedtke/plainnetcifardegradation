#ADAPTED FROM: 
#	https://github.com/lambdal/TensorFlow2-tutorial
# AND
# 	https://github.com/tensorflow/models/blob/master/official/benchmark/models/resnet_cifar_main.py
# AND
# 	https://github.com/keras-team/keras/blob/master/examples/cifar10_resnet.py
# AND
# 	https://github.com/chao-ji/tf-resnet-cifar10

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import functools
import tensorflow as tf
from tensorflow.python.keras import backend
from tensorflow.python.keras import layers

#BATCH_NORM_EPSILON = 1e-5
BATCH_NORM_EPSILON = 1e-3
BATCH_NORM_DECAY = 0.999
L2_WEIGHT_DECAY = 0.0001

"""The identity block is the block that has no conv layer at shortcut.
Arguments:
input_tensor: input tensor
kernel_size: default 3, the kernel size of
	middle conv layer at main path
filters: list of integers, the filters of 3 conv layer at main path
stage: integer, current stage label, used for generating layer names
block: current block label, used for generating layer names
training: Only used if training keras model with Estimator.  In other
  scenarios it is handled automatically.
Returns:
Output tensor for the block.
"""
def identity_building_block(input_tensor,
							kernel_size,
							filters,
							stage,
							block,
							training=None):
	
	filters1, filters2 = filters
	if tf.keras.backend.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1
	conv_name_base = 'res' + str(stage) + block + '_branch'
	bn_name_base = 'bn' + str(stage) + block + '_branch'

	#x = tf.keras.layers.Dropout(0.1)(input_tensor)
	x = tf.keras.layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '2a',
										 momentum=BATCH_NORM_DECAY, epsilon=BATCH_NORM_EPSILON)(input_tensor, training=training)

	x = tf.keras.layers.Activation('relu')(x)

	x = tf.keras.layers.Conv2D(filters1, kernel_size,
							 padding='same',
							 kernel_initializer='he_normal',
							 kernel_regularizer=
							 tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
							 use_bias=False,
							 name=conv_name_base + '2a')(x)
	#x = tf.keras.layers.Dropout(0.15)(x)
	x = tf.keras.layers.BatchNormalization(axis=bn_axis,
										 name=bn_name_base + '2b',
										 momentum=BATCH_NORM_DECAY,
										 epsilon=BATCH_NORM_EPSILON)(
											 x, training=training)
	x = tf.keras.layers.Activation('relu')(x)
	x = tf.keras.layers.Conv2D(filters2, kernel_size,
							 padding='same',
							 kernel_initializer='he_normal',
							 kernel_regularizer=
							 tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
							 use_bias=False,
							 name=conv_name_base + '2b')(x)
	x = tf.keras.layers.add([x, input_tensor])
	#x = tf.keras.layers.Activation('relu')(x)
	return x

"""A block that has a conv layer at shortcut.
Arguments:
input_tensor: input tensor
kernel_size: default 3, the kernel size of
	middle conv layer at main path
filters: list of integers, the filters of 3 conv layer at main path
stage: integer, current stage label, used for generating layer names
block: current block label, used for generating layer names
strides: Strides for the first conv layer in the block.
training: Only used if training keras model with Estimator.  In other
  scenarios it is handled automatically.
Returns:
Output tensor for the block.
Note that from stage 3,
the first conv layer at main path is with strides=(2, 2)
And the shortcut should have strides=(2, 2) as well
"""
def conv_building_block(input_tensor,
						kernel_size,
						filters,
						stage,
						block,
						strides=(2, 2),
						training=None):
	
	filters1, filters2 = filters
	if tf.keras.backend.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1
	conv_name_base = 'res' + str(stage) + block + '_branch'
	bn_name_base = 'bn' + str(stage) + block + '_branch'
	#x = tf.keras.layers.Dropout(0.08)(input_tensor)
	
	x = tf.keras.layers.BatchNormalization(axis=bn_axis,
										 name=bn_name_base + '2a',
										 momentum=BATCH_NORM_DECAY,
										 epsilon=BATCH_NORM_EPSILON)(input_tensor, training=training)
	x = tf.keras.layers.Activation('relu')(x)
	
	x = tf.keras.layers.Conv2D(filters1, kernel_size, strides=strides,
							 padding='same',
							 kernel_initializer='he_normal',
							 kernel_regularizer=tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
							 use_bias=False,
							 name=conv_name_base + '2a')(x)
	#x = tf.keras.layers.Dropout(0.1)(x)
	
	x = tf.keras.layers.BatchNormalization(axis=bn_axis,
										 name=bn_name_base + '2b',
										 momentum=BATCH_NORM_DECAY,
										 epsilon=BATCH_NORM_EPSILON)(x, training=training)
	x = tf.keras.layers.Activation('relu')(x)

	x = tf.keras.layers.Conv2D(filters2, kernel_size, padding='same',
									kernel_initializer='he_normal',
									kernel_regularizer= tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
									use_bias=False,
									name=conv_name_base + '2b')(x)

	shortcut = tf.keras.layers.Conv2D(filters2, (1, 1), strides=strides,
									kernel_initializer = 'he_normal',
									kernel_regularizer = tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
									use_bias=False,
									name=conv_name_base + '1')(input_tensor)

	shortcut = tf.keras.layers.BatchNormalization(axis=bn_axis, name=bn_name_base + '1',momentum=BATCH_NORM_DECAY, epsilon=BATCH_NORM_EPSILON)(shortcut, training=training)
	x = tf.keras.layers.add([x, shortcut])
	#x = tf.keras.layers.Activation('relu')(x)
	return x


	"""A block which applies conv followed by multiple identity blocks.
	Arguments:
	input_tensor: input tensor
	size: integer, number of constituent conv/identity building blocks.
	A conv block is applied once, followed by (size - 1) identity blocks.
	kernel_size: default 3, the kernel size of
		middle conv layer at main path
	filters: list of integers, the filters of 3 conv layer at main path
	stage: integer, current stage label, used for generating layer names
	conv_strides: Strides for the first conv layer in the block.
	training: Only used if training keras model with Estimator.  In other
	scenarios it is handled automatically.
	Returns:
	Output tensor after applying conv and identity blocks.
	"""
def resnet_block(input_tensor,
				 size,
				 kernel_size,
				 filters,
				 stage,
				 conv_strides=(2, 2),
				 training=None):
	x = conv_building_block(input_tensor, kernel_size, filters, stage=stage,
						  strides=conv_strides, block='block_0',
						  training=training)
	for i in range(size - 1):
		#x = tf.keras.layers.Dropout(0.2)(x)
		x = identity_building_block(x, kernel_size, filters, stage=stage,
								block='block_%d' % (i + 1), training=training)
	return x




"""Instantiates the ResNet architecture.
	Arguments:
	num_blocks: integer, the number of conv/identity blocks in each block.
		The ResNet contains 3 blocks with each block containing one conv block
		followed by (layers_per_block - 1) number of idenity blocks. Each
		conv/idenity block has 2 convolutional layers. With the input
		convolutional layer and the pooling layer towards the end, this brings
		the total size of the network to (6*num_blocks + 2)
	classes: optional number of classes to classify images into
	training: Only used if training keras model with Estimator.  In other
	scenarios it is handled automatically.
	Returns:
	A Keras model instance.
	"""
def resnet(num_blocks, img_input=None, classes=10, training=None, k=1):
	if backend.image_data_format() == 'channels_first':
		x = layers.Lambda(lambda x: backend.permute_dimensions(x, (0, 3, 1, 2)),
						  name='transpose')(img_input)
		bn_axis = 1
	else:  # channel_last
		x = img_input
		bn_axis = 3

	x = tf.keras.layers.ZeroPadding2D(padding=(1, 1), name='conv1_pad')(x)
	x = tf.keras.layers.Conv2D(16, (3, 3),
								 strides=(1, 1),
								 padding='valid',
								 kernel_initializer='he_normal',
								 kernel_regularizer=
								 tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
								 use_bias=False,
								 name='conv1')(x)
	#x = tf.keras.layers.BatchNormalization(axis=bn_axis, name='bn_conv1',
	#										 momentum=BATCH_NORM_DECAY,
	#										 epsilon=BATCH_NORM_EPSILON)(
	#											 x, training=training)
	#x = tf.keras.layers.Activation('relu')(x)
	#x = tf.keras.layers.Dropout(0.05)(x)

	x = resnet_block(x, size=num_blocks, kernel_size=3, filters=[16*k, 16*k],
						stage=2, conv_strides=(1, 1), training=training)

	#x = tf.keras.layers.Dropout(0.15)(x)

	x = resnet_block(x, size=num_blocks, kernel_size=3, filters=[32*k, 32*k],
						stage=3, conv_strides=(2, 2), training=training)

	#x = tf.keras.layers.Dropout(0.15)(x)

	x = resnet_block(x, size=num_blocks, kernel_size=3, filters=[64*k, 64*k],
						stage=4, conv_strides=(2, 2), training=training)

	x = tf.keras.layers.GlobalAveragePooling2D(name='avg_pool')(x)
	#x = tf.keras.layers.Dropout(0.5)(x)
	x = tf.keras.layers.Dense(classes, activation='softmax',
								kernel_initializer='he_normal',
								kernel_regularizer=tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
								use_bias=False,
								name='fc10')(x)

	inputs = img_input
	# Create model.
	model = tf.keras.models.Model(inputs, x, name='resnetX')
	return model

resnet8_1 = functools.partial(resnet, num_blocks=1, k=1)
resnet8_2 = functools.partial(resnet, num_blocks=1, k=2)
resnet8_3 = functools.partial(resnet, num_blocks=1, k=3)

resnet14_1 = functools.partial(resnet, num_blocks=2, k=1)
resnet14_2 = functools.partial(resnet, num_blocks=2, k=2)

resnet20_1 = functools.partial(resnet, num_blocks=3, k=1)
resnet20_2 = functools.partial(resnet, num_blocks=3, k=2)

resnet26_1 = functools.partial(resnet, num_blocks=4, k=1)
resnet26_2 = functools.partial(resnet, num_blocks=4, k=2)
resnet26_3 = functools.partial(resnet, num_blocks=4, k=3)

resnet32_1 = functools.partial(resnet, num_blocks=5, k=1)
resnet32_2 = functools.partial(resnet, num_blocks=5, k=2)

resnet38_1 = functools.partial(resnet, num_blocks=6, k=1)
resnet38_2 = functools.partial(resnet, num_blocks=6, k=2)
resnet38_3 = functools.partial(resnet, num_blocks=6, k=3)
resnet38_4 = functools.partial(resnet, num_blocks=6, k=4)

resnet44_1 = functools.partial(resnet, num_blocks=7, k=1)
resnet44_2 = functools.partial(resnet, num_blocks=7, k=2)

resnet50_1 = functools.partial(resnet, num_blocks=8, k=1)
resnet50_2 = functools.partial(resnet, num_blocks=8, k=2)

resnet56_1 = functools.partial(resnet, num_blocks=9, k=1)
resnet56_2 = functools.partial(resnet, num_blocks=9, k=2)

resnet62_1 = functools.partial(resnet, num_blocks=10, k=1)
resnet62_2 = functools.partial(resnet, num_blocks=10, k=2)
'''
67 - 11
73 - 12
79 - 13
85 - 14
91 - 15
97 - 16

'''
#---

resnet98_1 = functools.partial(resnet, num_blocks=16, k=1)
resnet98_2 = functools.partial(resnet, num_blocks=16, k=2)