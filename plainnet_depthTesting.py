#ADAPTED FROM: 
#	https://github.com/lambdal/TensorFlow2-tutorial
# AND
# 	https://github.com/tensorflow/models/blob/master/official/benchmark/models/resnet_cifar_main.py
# AND
# 	https://github.com/keras-team/keras/blob/master/examples/cifar10_resnet.py

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import functools
import tensorflow as tf
from tensorflow.python.keras import backend
from tensorflow.python.keras import layers

BATCH_NORM_DECAY = 0.992
BATCH_NORM_EPSILON = 1e-3
L2_WEIGHT_DECAY = 2e-4

"""
Returns:
Output tensor for the block.
"""
def conv_plain_layer(input_tensor, kernel_size, filterF, stage, training=None):
	if tf.keras.backend.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1
	
	x = tf.keras.layers.Conv2D(filterF, kernel_size,
							 padding='same',
							 kernel_initializer='he_normal',
							 kernel_regularizer=
							 tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
							 use_bias=False)(input_tensor)
	x = tf.keras.layers.BatchNormalization(axis=bn_axis,
										 momentum=BATCH_NORM_DECAY,
										 epsilon=BATCH_NORM_EPSILON)(x, training=training)
	x = tf.keras.layers.Activation('relu')(x)
	return x

"""
Conv layer designed to reduce dimensions in width and height (by using stride 2,2)
Returns:
Output tensor for the block.
"""
def conv_reduct_layer(input_tensor, kernel_size, filterF, stage, strides=(2, 2), training=None):
	if tf.keras.backend.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1

	x = tf.keras.layers.Conv2D(filterF, kernel_size, strides=strides, padding='same',
									kernel_initializer='he_normal',
									kernel_regularizer= tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
									use_bias=False)(input_tensor)

	x = tf.keras.layers.BatchNormalization(axis=bn_axis,
										 momentum=BATCH_NORM_DECAY,
										 epsilon=BATCH_NORM_EPSILON)(x, training=training)

	x = tf.keras.layers.Activation('relu')(x)
	return x


"""
Plain network layer group
Returns:
Output tensor after applying conv and identity blocks.
"""
def plainnet_group(input_tensor, size, kernel_size, filterF, stage, conv_strides=(2, 2), training=None):
	x = conv_reduct_layer(input_tensor, kernel_size, filterF, stage=stage, strides=conv_strides, training=training)
	for i in range(size - 1):
		x = conv_plain_layer(x, kernel_size, filterF, stage=stage, training=training)
	return x


"""
Instantiates the architecture.
Arguments:
factor_N: Factor N
Returns:
A Keras model instance.
"""
def plainnet(factor_N, img_input=None, classes=10, training=None):
	if backend.image_data_format() == 'channels_first':
		x = layers.Lambda(lambda x: backend.permute_dimensions(x, (0, 3, 1, 2)),
						  name='transpose')(img_input)
	else:  # channel_last
		x = img_input

	x = tf.keras.layers.ZeroPadding2D(padding=(1, 1), name='conv1_pad')(x)
	x = tf.keras.layers.Conv2D(16, (3, 3),
								 strides=(1, 1),
								 padding='valid',
								 kernel_initializer='he_normal',
								 kernel_regularizer=
								 tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
								 use_bias=False,
								 name='conv1')(x)

	x = plainnet_group(x, size=factor_N, kernel_size=3, filterF=16,
						stage=2, conv_strides=(1, 1), training=training)

	x = plainnet_group(x, size=factor_N, kernel_size=3, filterF=32,
						stage=3, conv_strides=(2, 2), training=training)

	x = plainnet_group(x, size=factor_N, kernel_size=3, filterF=64,
						stage=4, conv_strides=(2, 2), training=training)

	x = tf.keras.layers.GlobalAveragePooling2D(name='avg_pool')(x)

	x = tf.keras.layers.Dense(classes, activation='softmax',
								kernel_initializer='he_normal',
								kernel_regularizer=tf.keras.regularizers.l2(L2_WEIGHT_DECAY),
								use_bias=False,
								name='fc10')(x)

	inputs = img_input
	# Create model.
	model = tf.keras.models.Model(inputs, x, name='plainnetX')
	return model


plainnet_A = functools.partial(plainnet, factor_N=6)
plainnet_B = functools.partial(plainnet, factor_N=12)