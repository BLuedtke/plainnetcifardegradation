#ADAPTED FROM: 
#	https://github.com/lambdal/TensorFlow2-tutorial
# AND
# 	https://github.com/tensorflow/modelAs/blob/master/official/benchmark/modelAs/resnet_cifar_main.py
# AND
# 	https://github.com/keras-team/keras/blob/master/examples/cifar10_resnet.py
# AND
# 	https://github.com/chao-ji/tf-resnet-cifar10

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import datetime
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.callbacks import TensorBoard, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
import plainnet_depthTesting
import numpy as np
import matplotlib.pyplot as pyplot
import matplotlib.ticker as ticker

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

NUM_GPUS = 1
BS_PER_GPU = 64
#NUM_EPOCHS = 70
NUM_EPOCHS = 90

HEIGHT = 32
WIDTH = 32
NUM_CHANNELS = 3
NUM_CLASSES = 10
NUM_TRAIN_SAMPLES = 50000

BASE_LEARNING_RATE = 0.015
#BASE_LEARNING_RATE = 0.02

(x_train, y_train), (x_test, y_test) = keras.datasets.cifar10.load_data()
# Input image dimensions.
input_shape = x_train.shape[1:]
# Normalize data.
x_train = x_train.astype('float32') / 255
x_test = x_test.astype('float32') / 255

'''
#subtract pixel mean
x_train_mean = np.mean(x_train, axis=0)
x_train -= x_train_mean
x_test -= x_train_mean
'''
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')
print('y_train shape:', y_train.shape)

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, NUM_CLASSES)
y_test = keras.utils.to_categorical(y_test, NUM_CLASSES)
# This will do preprocessing and realtime data augmentation:
datagen = ImageDataGenerator(
	featurewise_center=False,		# set input mean to 0 over the dataset
	samplewise_center=False,		# set each sample mean to 0
	featurewise_std_normalization=False,	# divide inputs by std of dataset
	samplewise_std_normalization=False,		# divide each input by its std
	zca_whitening=False,			# apply ZCA whitening
	zca_epsilon=1e-06,				# epsilon for ZCA whitening
	rotation_range=0,				# randomly rotate images in the range (deg 0 to 180)
	width_shift_range=0.1,			# randomly shift images horizontally
	height_shift_range=0.1,			# randomly shift images vertically
	shear_range=0.,					# set range for random shear
	zoom_range=0.,					# set range for random zoom
	channel_shift_range=0.,			# set range for random channel shifts
	fill_mode='nearest',			# set mode for filling points outside the input boundaries
	cval=0.,						# value used for fill_mode = "constant"
	horizontal_flip=True,			# randomly flip images
	vertical_flip=False,			# randomly flip images	
	rescale=None,					# set rescaling factor (applied before any other transformation)
	preprocessing_function=None,	# set function that will be applied on each input
	data_format=None,				# image data format, either "channels_first" or "channels_last"
	validation_split=0.0)			# fraction of images reserved for validation (strictly between 0 and 1)

# Compute quantities required for featurewise normalization
# (std, mean, and principal components if ZCA whitening is applied).
datagen.fit(x_train)

img_input = tf.keras.layers.Input(shape=input_shape)

#opt = keras.optimizers.SGD(learning_rate=BASE_LEARNING_RATE, momentum=0.9)
opt = tf.keras.optimizers.Adam(
	learning_rate=BASE_LEARNING_RATE,
	beta_1=0.9,
	beta_2=0.999,
	epsilon=0.025,
	amsgrad=False,
	name="Adam"
)
opt2 = tf.keras.optimizers.Adam(
	learning_rate=BASE_LEARNING_RATE,
	beta_1=0.9,
	beta_2=0.999,
	epsilon=0.025,
	amsgrad=False,
	name="Adam"
)

modelA = plainnet_depthTesting.plainnet_A(img_input=img_input, classes=NUM_CLASSES)
modelB = plainnet_depthTesting.plainnet_B(img_input=img_input, classes=NUM_CLASSES)

modelA.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
modelB.compile(optimizer=opt2, loss='categorical_crossentropy', metrics=['accuracy'])

log_dir="logs/fit/plain/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
file_writer = tf.summary.create_file_writer(log_dir + "/metrics")
file_writer.set_as_default()

tensorboard_callback = TensorBoard(log_dir=log_dir,update_freq='epoch',histogram_freq=1)
tensorboard_callback2 = TensorBoard(log_dir=log_dir,update_freq='epoch',histogram_freq=1)

lr_reducer = ReduceLROnPlateau(factor=0.05,cooldown=0,patience=8,min_lr=0.5e-6)
lr_reducer2 = ReduceLROnPlateau(factor=0.05,cooldown=0,patience=8,min_lr=0.5e-6)
# Fit the modelA on the batches generated by datagen.flow().
history = modelA.fit(datagen.flow(x_train, y_train, batch_size=BS_PER_GPU),
					validation_data=(x_test, y_test),
					epochs=NUM_EPOCHS, verbose=1, workers=4,validation_freq=1,
					callbacks=[tensorboard_callback, lr_reducer])

log_dir="logs/fit/plain/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
file_writer2 = tf.summary.create_file_writer(log_dir + "/metrics")
file_writer2.set_as_default()
historyB = modelB.fit(datagen.flow(x_train, y_train, batch_size=BS_PER_GPU),
					validation_data=(x_test, y_test),
					epochs=NUM_EPOCHS, verbose=1, workers=4,validation_freq=1,
					callbacks=[tensorboard_callback2, lr_reducer2])

# plot accuracy during training
tick_spacing = 5
ax = pyplot.subplot(111)
pyplot.title('Accuracy')
pyplot.plot(history.history['accuracy'], label='CNN_A_train',color='red', linestyle='dashed', linewidth=2.0)
pyplot.plot(history.history['val_accuracy'], label='CNN_A_test',color='red', linewidth=2.0)
pyplot.plot(historyB.history['accuracy'], label='CNN_B_train',color='blue', linestyle='dashed', linewidth=2.0)
pyplot.plot(historyB.history['val_accuracy'], label='CNN_B_test',color='blue', linewidth=2.0)
ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
pyplot.legend()
pyplot.show()
try:
	pyplot.savefig("graph.pdf")
	pyplot.savefig("graph.png")
	pyplot.savefig("graph.svg")
except Exception as e:
	print(e)
modelA.save('modelA.plainA2')
modelB.save('modelB.plainB2')